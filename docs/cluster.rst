Cluster
=========

Cluster is used to gather information about the PSM cluster.

API Methods
-----------

.. automethod:: pensando.PSM.getClusterInfo

Data Structures
---------------

None at this time

Examples
--------

.. literalinclude:: ../examples/psm_examples.py
    :language: python
    :start-after: # EXAMPLE getClusterInfo()
    :end-before: # END getClusterInfo()




.. |br| raw:: html

   <br />
