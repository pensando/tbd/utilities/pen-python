Events
============

Get all event objects from PSM


API Methods
-----------

.. automethod:: pensando.PSM.getEvents


Data Structures
---------------

None defined


Examples
--------

Get info on all Events generated on PSM

.. literalinclude:: ../examples/psm_examples.py
    :language: python
    :start-after: # EXAMPLE getEvents()
    :end-before: # END getEvents()




.. |br| raw:: html

   <br />
