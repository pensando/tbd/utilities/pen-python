Overview
=========

The Pensando Python API is a wrapper around the Pensando Policy Services Manager
(PSM) server's REST API, making it easier (and more pythonic) for Python users
to build software that usesthe Pensando platform.

The data structures and methods available in this library correspond with
what's described in the REST API Reference located on your PSM cluster @
https://<your PSM IP>/docs/generated/apiref/

.. note::
    If you find an issue or would like a currently un-implemented part of the API to
    be considered for implementation, you can open an `issue <https://gitlab.com/pensando/pen-python/-/issues/new?issue>`_
    or send an `email <mailto:incoming+pensando-pen-python-22338655-issue-@incoming.gitlab.com>`_ and have an issue
    automatically opened for you.

The documentation is split into categories, each of which have descriptions for
data structures and methods. Some example code is also provided.

The Pensando Python API is used by the SE team and all examples are used as part
of the CI testing gates. As a result it is tested heavily internally before release.


.. |br| raw:: html

   <br />
