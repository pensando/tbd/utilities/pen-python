Security
=========

Configure and manage security features like Security Groups, Rules, Certificates etc.

API Objects are: App FirewallProfile NetworkSecurityPolicy SecurityGroup


API Methods
-----------

.. automethod:: pensando.PSM.getNetworkSecurityPolicies

.. automethod:: pensando.PSM.getNetworkSecurityPolicy


Data Structures
---------------

None at this time

Examples
--------

Get info on all Network Security Policies

.. literalinclude:: ../examples/psm_examples.py
    :language: python
    :start-after: # EXAMPLE getNetworkSecurityPolicies()
    :end-before: # END getNetworkSecurityPolicies()




.. |br| raw:: html

   <br />
