#!/usr/bin/env python
import json
import click
import os
import sys
import logging

# This allows us to import the PSM class from the lib directory just for these examples
libpath = os.path.dirname(os.path.abspath(__file__))
sys.path[:0] = [os.path.join(libpath, os.pardir, 'lib')]
from pensando import logger
from pensando.psmapi import PSM, PenConfig
from pprint import pprint as pp
from pensando.psm_modules.utils import *

# # Dummy function to simulate fetching a JSON object from an external system
# def fetch_json_from_system(system_name):
#     # Simulated external system response
#     external_response = {
#         "category": "workload",
#         "kind": "workloads",
#         "method": "GET",
#         "response": {
#             "kind": "Workload",
#             "api-version": "v1",
#             "meta": {
#                 "name": "orch1--vm-1028",
#                 "tenant": "default",
#                 "namespace": "default",
#                 "generation-id": "2",
#                 "resource-version": "54521395",
#                 "uuid": "7249979d-bed8-49d0-a1e6-df08f86a8061",
#                 "labels": {
#                     "app.func.1": "EDI",
#                     "app.func.2": "stream_processing",
#                     "app.func.3": "telemetry",
#                     "app.group.1": "kafka",
#                     "app.group.2": "broker",
#                     "app.name": "otto",
#                     "app.type.1": "broker",
#                     "com.amd.namespace": "tbd",
#                     "com.amd.orch-name.vcenter": "tbd-vsphere",
#                     "com.amd.orchestrator.display-name": "otto-v10.12___(22.50)",
#                     "host.name": "broker-dev",
#                     "orch.type": "vcenter"
#                 },
#                 "creation-time": "2023-05-22T17:17:22.974408117Z",
#                 "mod-time": "2024-05-22T14:50:04.929976596Z",
#                 "self-link": "/configs/workload/v1/tenant/default/workloads/orch1--vm-1028"
#             },
#             "spec": {
#                 "host-name": "orch1--host-37"
#             },
#             "status": {
#                 "mirror-sessions": None,
#                 "propagation-status": {
#                     "status": "",
#                     "endpoint-prop-status": None,
#                     "workload-propagation-status": {
#                         "generation-id": "2",
#                         "updated": 0,
#                         "pending": 0,
#                         "status": "",
#                         "pending-dses": None,
#                         "dse-status": None,
#                         "pdt-status": None
#                     }
#                 }
#             }
#         },
#         "url": ":/configs/workload/v1/tenant/default/workloads/orch1--vm-1028",
#         "id": 5
#     }

#     # Return the JSON object inside the "response" section
#     return external_response["response"]

class WorkloadCRUD:
    def __init__(self, workload=None):
        self.workload = workload or {}

    def transform_labels(self):

        for system_name, item in self.workload.items():
            labels = {
                "HostName": item.get("hostname", "Unknown"),
                "AppType": item.get("apptype", "Unknown"),
                "AppName": item.get("appname", "Unknown"),
                "Deployment": item.get("deployment", "NA"),
                "Category": item.get("category", "NA"),
                "MgmtIP": item.get("mgmtip", "Unknown"),
                "InternalIP": item.get("intip", "Unknown")
            }

        try:
            resp = myPSM.updateWorkloadLabels(system_name,labels)
            return True
        except Exception as e:
            logger.error(f"Unable to retrieve workload info: {e} for {system_name}")
            return False


    def merge_with_external(self, transformed_labels):
        merged_configs = {}
        for system_name, new_labels in transformed_labels.items():
            fetched_json = fetch_json_from_system(system_name)

            # Ensure the 'meta' and 'labels' sections exist
            if 'meta' not in fetched_json:
                fetched_json['meta'] = {}
            if 'labels' not in fetched_json['meta']:
                fetched_json['meta']['labels'] = {}

            # Merge the new labels into 'meta.labels'
            fetched_json['meta']['labels'].update(new_labels)

            # Only include the labels section in the output
            merged_configs[system_name] = fetched_json['meta']['labels']
        return merged_configs

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
def wkld(input_file):
    """CLI application to process workload JSON file."""
    goList = list()
    noGoList = list()
    # Load the input JSON file
    with open(input_file, 'r') as file:
        input_json = json.load(file)

    # Loop through each item in the input JSON
    for system_name, details in input_json.items():
        # Create an instance of WorkloadCRUD with the single item as input
        workload_crud = WorkloadCRUD({system_name: details})

        # Transform the labels
        if workload_crud.transform_labels():
            goList.append(system_name)
        else:
            noGoList.append(system_name)

    print(f"\n\nSuccessfully updated labels for the following:")
    print("="*50)
    for system in goList:
        print(f"{system}")

    print(f"\n\nUnable to update labels for the following:")
    print("="*50)
    for system in noGoList:
        print(f"{system}")

        # Merge the transformed labels with external JSON objects
        # merged_config = workload_crud.merge_with_external(transformed_labels)

        # Print the merged labels section for the current item
        #print(json.dumps({system_name: merged_config[system_name]}, indent=4))

if __name__ == "__main__":
    homeDir = os.path.expanduser('~')
    logger.setLevel(logging.DEBUG)
    myPSM = PSM(server="https://10.10.100.212",user='admin',password="Pensando0$",tenant='default')

    wkld()
