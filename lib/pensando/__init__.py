#!/usr/bin/env python
# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ryan Tischer ryan@pensando.io
#            Edward Arcuri edward@pensando.io
#
#

import logging

from .psmapi import PSM

# Pensando API version
__version__ = '0.1.0'


class PenLogFormatter(logging.Formatter):
    width = 20
    datefmt = '%Y-%m-%d %H:%M:%S'

    def format(self, record):
        cpath = f'{record.funcName}({record.lineno})'
        # cpath = cpath[-self.width:].ljust(self.width)
        # record.message = record.getMessage()
        levelName = f"[{record.levelname}]"
        outputString = (f"{levelName},"
                        f"{self.formatTime(record, self.datefmt)},{cpath},"
                        f"\"{record.getMessage()}\"")

        if record.exc_info:
            # Cache the traceback text to avoid converting it multiple times
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            if outputString[-1:] != "\n":
                outputString = outputString + "\n"
            outputString = outputString + record.exc_text
        return outputString


logger = logging.getLogger("pensando-api")
# logger.setLevel(logging.DEBUG)
strmHandler = logging.StreamHandler()
formatter = PenLogFormatter()
strmHandler.setFormatter(formatter)
logger.addHandler(strmHandler)

__all__ = [
    "PSM",
    "DEFAULT_TIMEOUT",
]
