# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
import logging

from .base import PSMBase


class AuthModule(PSMBase):
    """
    Provides stubs for calling APIs related to auth endpoints in PSM
    """
    def getAuthenticationPolicy(self):
        """
        Get the authentication policy for the PSM cluster

        :return: Authentication Policy
        :rtype: json
        """
        url = f"{self.server}/configs/auth/v1/authn-policy"
        return self._get_json(url).json()

    def getRoleBindings(self):
        """
        Get the role bindings for the users

        :return: Role bindings for all users
        :rtype: json
        """
        url = f"{self.server}/configs/auth/v1/tenant/{self.tenant}/role-bindings"
        logging.debug(f"Using the tenant of {self.tenant}")
        return self._get_json(url).json()

    def getUser(self,user):
        """
        Get info dump for particular user

        :return: Information on user specified
        :rtype: json
        """
        url = f"{self.server}/configs/auth/v1/tenant/{self.tenant}/users/{user}"
        logging.debug(f"Using the tenant of {self.tenant} for user {user}")
        return self._get_json(url).json()

    def getUsers(self):
        """
        Get info dump of all users

        :return: Information on all users in tenant
        :rtype: json
        """
        url = f"{self.server}/configs/auth/v1/tenant/{self.tenant}/users"
        logging.debug(f"Using the tenant of {self.tenant}")
        return self._get_json(url).json()

    def getUserPrefs(self,user):
        """
        Get the user preferences for the specified user

        :param user: username of user to get preferences for
        :type user: string
        :return: user preferences
        :rtype: json
        """
        url = f"{self.server}/configs/auth/v1/tenant/{self.tenant}/user-preferences/{user}"
        logging.debug(f"Using the tenant of {self.tenant} and user of {user}")
        return self._get_json(url).json()
