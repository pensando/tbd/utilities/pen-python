# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
import logging

from .base import PSMBase


class ClusterModule(PSMBase):
    """
    Provides stubs for calling APIs related to cluster endpoints in PSM
    """

    def getClusterInfo(self):
        """
        getCluster Get information about PSM cluster

        :return: cluster info
        :rtype: json
        """
        logging.debug(f"Gathering cluster info from {self.server}")
        url = f"{self.server}/configs/cluster/v1/cluster"
        return self._get_json(url).json()

    def getDSCs(self):
        """
        getDSCs Get all information about all DSCs managed by PSM

        :return: All information about all DSCs
        :rtype: json
        """
        logging.debug(f"Getting all DSC info from {self.server}")
        url = f"{self.server}/configs/cluster/v1/distributedservicecards"
        return self._get_json(url).json()

    def getDSCMacs(self):
        """
        getDSCMacs Get the mac addys of all DSCs managed by PSM

        :return: List of mac addresses of all DSCs managed by PSM
        :rtype: list
        """
        logging.debug(f"Getting all DSC macs from {self.server}")
        macList = list()
        url = f"{self.server}/configs/cluster/v1/distributedservicecards"
        response = self._get_json(url).json()
        for item in response['items']:
            macList.append(item['meta']['name'])
        return macList
