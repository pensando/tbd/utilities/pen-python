# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
from .base import PSMBase


class SecurityModule(PSMBase):
    """
    Provides stubs for calling APIs related to security endpoints in PSM
    """

    def getNetworkSecurityPolicies(self):
        """
        getNetworkSecurityPolicies Get all network security policies (FW) for the tenant

        :return: All information about network security policies defined for tenant
        :rtype: json
        """
        url = f"{self.server}/configs/security/v1/tenant/{self.tenant}/networksecuritypolicies"
        return self._get_json(url).json()

    def getNetworkSecurityPolicy(self,policyName):
        """
        getNetworkSecurityPolicy Get information about specificed network security policy

        :param policyName: Network security policy name
        :type policyName: string
        :return: Information about policy specified
        :rtype: json
        """
        url = f"{self.server}/configs/security/v1/tenant/{self.tenant}/networksecuritypolicies/{policyName}"
        return self._get_json(url).json()
