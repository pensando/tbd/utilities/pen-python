# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
import json
import logging

from .utils import calcDate
from .base import PSMBase


class TelemetryModule(PSMBase):
    """
    Provides stubs for calling APIs related to telemetry and metrics in PSM
    """
    def getMets(self):
        url = f"{self.server}/telemetry/v1/metrics"
        return self._post_json(url).json()

    def getMetrics(self,**kwargs):
        """
        Gather specified metrics from PSM cluster

        .. note::
            This method requires either sending a pre-built Data Structure (ds) OR at least the
            type of metric (mt) and a fields list (fl) for the type of metric.  If the Data
            Structure is sent and other parameters are also sent, the Data Structure takes
            precedence and all other parameters are ignored

        :param ds: getMetrics Data Structure. |br|
                    If the other parameters do not work for your solution you can send
                    your own serialized json document and that will be used instead of
                    the one built by this method |br| Defaults to None. |br|
                    If this is used, all other parameters are ignored.
        :type ds: json, optional, default is None

        :param mt: The kind of metric to gather, valid types are: |br|
                    - Cluster |br|
                    - DropMetrics |br|
                    - LifMetrics |br|
                    - MacMetrics |br|
                    - MgmtMacMetrics |br|
                    - SessionSummaryMetrics |br|
        :type mt: string, required if Data Structure (ds) is not sent, default is None

        :param fl: Fields to gather for the metric type, defaults to None |br|
                    (Use the 'Create Graph' wizard in PSM to understand these fields)
        :type fl: list of strings, required if Data Structure (ds) is not sent and metric type (mt) is defined

        :param func: Function
        :type func:  string, optional, default is 'last'

        :param st: start time of query Either: |br| - Number of days in past to start query |br|
                    - Formatted datetime of %Y-%m-%dT%H:%M:%SZ to start the query
        :type st:  string, optional, default is 1 day

        :param et: end time of the query as either: |br| - Number of days in the future to end query |br|
                    - Formatted datetime of %Y-%m-%dT%H:%M:%SZ to end the query
        :type et:  string, optional, default is current time, i.e. 'now'

        :param gt: group by time
        :type gt:  optional, default is 60m

        :param so: sort order, either 'ascending' or 'descending'
        :type gt:  string, optional, default is 'ascending'

        :raises Exception: Invalid metric type - refer to list above
        :raises Exception: Invalid fields list - the list must contain at least 1 parameter
                                                 relevant to the selected metric type

        :return: Message from PSM server
        :rtype: json

        """

        url = f"{self.server}/telemetry/v1/metrics"
        dataStructure = kwargs.get('ds',None)

        if dataStructure is None:
            keyList = ('queries','function','start-time','end-time','group-by-time','sort-order')
            metricType = kwargs.get('mt',None)
            fieldsList = kwargs.get('fl', None)

            func = kwargs.get('function', 'last')
            start = kwargs.get('st', 1)
            end = kwargs.get('et', 'now')
            groupByTime = kwargs.get('gt', '60m')
            sortOrder = kwargs.get('so', 'ascending')

            # Do some checking to make sure we have what we need to perform the query
            validTypes = ['Cluster','DropMetrics','LifMetrics','MacMetrics','MgmtMacMetrics','SessionSummaryMetrics']
            if metricType not in validTypes:
                raise Exception(f"metricType must be one of the following: {validTypes}")
            if fieldsList is None:
                raise Exception(f"Must include at least 1 field in field list")

            # We were sent in a number of days as an int, otherwise it's a formatted date
            if isinstance(start, int):
                calcStart = calcDate('past',start,dateFormat='T')
            else:
                calcStart = start

            # We want the query to end 'now', 'end' days in the future or and explicit datetime
            if end == 'now':
                calcEnd = calcDate(end,'null',dateFormat='T')
            elif isinstance(end, int):
                calcEnd = calcDate('future',end,dateFormat='T')
            else:
                calcEnd = end

            # Build the request body
            valuesList = ([dict(kind=f'{metricType}',fields=[f'{fieldsList}'])],
                          func,calcStart,calcEnd,groupByTime,sortOrder)
            requestDict = dict(zip(keyList,valuesList))
            print(f"Sending {json.dumps(requestDict)} to _post_json ")
            logging.debug(f"Sending {json.dumps(requestDict)} to _post_json ")

            return self._post_json(url,json.dumps(requestDict)).json()
        else:
            jStructure = json.loads(dataStructure)
            qStructure = jStructure['queries'][0]
            fieldsList = qStructure['fields']
            fieldsList.insert(0,"time")

            response = self._post_json(url,dataStructure).json()
            for result in response['results']:
                for series in result['series']:
                    if "columns" in series:
                        del series['columns']
                    if "values" in series:
                        valuesList = list()
                        for item in series['values']:
                            metricsDict = {key: value for key, value in zip(fieldsList,item)}
                            valuesList.append(metricsDict)
                        series['values'] = valuesList
            return response
