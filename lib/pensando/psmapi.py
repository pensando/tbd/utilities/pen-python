#!/usr/bin/env python
# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ryan Tischer ryan@pensando.io
#            Edward Arcuri edward@pensando.io
#
#
import json
import logging
import pathlib
import requests
import configparser

from . import psm_modules
from urllib3.exceptions import InsecureRequestWarning


class PenConfig(dict):
    """
    PenConfig [summary]

    :param dict: [description]
    :type dict: [type]
    """

    def __init__(self, section, configFile):
        """
        __init__ intialization of Pensando config object

        :param section: The section in the configFile (~/.penrc default) to parse
        :type section: string
        :param configFile: Full path to config file to parse the section from
        :type configFile: string
        """
        def _parseConfig(section, configFile):
            """
            _parseConfig Parses the specified section in the config file

            :param section: The section in the configFile to parse
            :type section: string
            :param configFile: Full path to config file to parse
            :type configFile: string
            :return: Dict of all settings from config file
            :rtype: dict
            """
            cfgDict = {}
            try:
                config = configparser.RawConfigParser()
                config.read(f'{configFile}')
                cfgDict = dict(config.items(section))
                logging.debug(f"Paramters set for {section}:{cfgDict}")
            except configparser.NoSectionError as nse:
                logging.exception(f"{section} does not exist in {configFile}: {nse}")

            return cfgDict

        self.configFile = pathlib.Path(configFile)
        self.section = section
        self.confDict = {}
        if pathlib.Path.is_file(self.configFile):
            # logging.debug(f"{configFile} exists - setting parameters specified")
            self.confDict = _parseConfig(self.section,self.configFile)
        else:
            # logging.debug(f"{configFile} does not exist, using defaults")
            self.confDict = {'return':'defaults'}

    def getValue(self,item):
        return self.confDict[item]

    def getKeys(self):
        return self.confDict.keys()


class NewObject:
    def __init__(self, dict2Convert):
        self.__dict__.update(dict2Convert)


class PSM(psm_modules.WorkloadModule,
          psm_modules.TelemetryModule,
          psm_modules.AuthModule,
          psm_modules.ClusterModule,
          psm_modules.SecurityModule,
          psm_modules.MonitoringModule,
          psm_modules.ObjstoreModule,
          psm_modules.EventsModule,
          psm_modules.SysruntimeModule):

    """
    The psmapi module implements the PSM class.  It provides an
    interface to the REST API on any Pensando Policy Services Manager cluster.

    .. note::
        Instantiation of this class requires either sending the server url and
        login information or using a configuration file.  If neither server/login
        information is provided or a config file, then it will default to the
        .penrc file in the user's home directory and will read the PSM_API section
        from that file

    :param config: path to configuration file. |br|
                If this option is used, any other parameters are ignored
    :type config: file, optional, default is ~/.penrc

    :param server: url of PSM server to connect to
    :type server: http(s) string

    :param user: username to login to PSM server
    :type user: string

    :param password: password for user provided
    :type password: string

    :param tenant: tenant to log into on PSM server
    :type tenant:  string, must be specified if sending user/password

    :raises Exception: Attribute error: user not defined if server is defined
    :raises Exception: Attribute error: password not defined if server is defined

    :return: Login message from PSM server
    :rtype: json

    """

    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

    def __init__(self,**kwargs):

        super().__init__()

        # object configuration attributes
        for key, value in kwargs.items():
            logging.debug(key,value)
            setattr(self, key, value)

        # Check if neither config nor server is defined and set them to None if not.
        try:
            self.config
        except AttributeError:
            self.config = None
        try:
            self.server
        except AttributeError:
            self.server = None

        # If the user sent in a server, they also need to send in user, pw and tenant id.
        if self.server is not None:
            try:
                self.user
            except AttributeError:
                logging.error(f"You must define a user, password and tenant if you define the server")
                raise AttributeError
            try:
                self.password
            except AttributeError:
                logging.error(f"You must define a user, password and tenant if you define the server")
                raise AttributeError
            try:
                self.tenant
            except AttributeError:
                logging.error(f"You must define a user, password and tenant if you define the server")
                raise AttributeError

        # If object is initialized with no paramters, use the default .penrc file
        if self.server is None:
            if self.config is None:
                self.config = f"{str(pathlib.Path.home())}/.penrc"
            try:
                logging.debug(f"Creating PSM object with {self.config} configuration")
                psmDict = PenConfig('PSM_API',self.config)
                self.server = psmDict.getValue('server')
                self.user = psmDict.getValue('user')
                self.password = psmDict.getValue('password')
                self.tenant = psmDict.getValue('tenant')
            except Exception:
                raise FileNotFoundError
        # else:
        #     self.server = serverURL
        #     self.user = user
        #     self.password = password
        #     self.tenant = tenant

        self.session = requests.Session()
        self.__psmLogin()

    def __repr__(self):
        return "Object contents\n"

    def __psmLogin(self):
        """
        __psmLogin(): Hidden class method used to login to PSM specified and create the
        session for use by the object
        :return: Session object
        :rtype: object
        """
        authData = json.dumps({"username": self.user,"password": self.password,"tenant": self.tenant}).encode("utf-8")
        self.session.verify = False
        auth = self.session.post(f"{self.server}/v1/login",authData)
        logging.debug(f"Returned from login attempt to {self.server}: {auth.json}")
        return self.session

    def dict2Obj(self,jsonDict):
        """
        Helper method that will convert the json to an object.  Makes user code more readable

        :param jsonDict: the stringified version of the json - see example below
        :type dscDict: dictionary
        :return: object version of json
        :rtype: namedtuple

        .. code-block:: python

            # dscList has been retrieved already
            for dsc in dscList:
                dscObj = myPSM.dict2Obj(dsc)
                print(f"{dscObj.status.host},{dscObj.meta.name}")
        """
        return json.loads(json.dumps(jsonDict),object_hook=NewObject)


if __name__ == "__main__":
    pass
